import axios from "axios"

class SlackBridge {
    constructor() {
        this.slackToken = "xoxb-238364777202-1607841648610-PiwXkmF1Rkec2HjppYsSUUJR";
        this.url = "https://slack.com/api/chat.postMessage"
        this.channel = "#ps-watcher"
    }

    async send(message) {
        const res = await axios.post(this.url, {
          channel: this.channel,
          text: message,
        }, { headers: { authorization: `Bearer ${this.slackToken}` } });
    }
}

const bridge = new SlackBridge();

export function notifyInStock(url) {
    console.log(`In Stock. ${url}`);
    bridge.send(`In Stock. ${url}`);
}

export function notifyDetectError(url) {
    console.log(`Detect Error. ${url}`);
    bridge.send(`Detect Error. ${url}`);
}

export function notify(message) {
    console.log(message);
    bridge.send(message);
}


export default { notifyInStock, notifyDetectError, notify }