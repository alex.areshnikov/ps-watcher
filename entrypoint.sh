#!/bin/sh
set -e

cd /ps-watcher
yarn start

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"

while true; do sleep 1000; done
