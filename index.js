import puppeteer from "puppeteer-extra"
import StealthPlugin from "puppeteer-extra-plugin-stealth"
import cron from "node-cron"

import notifiers from "./lib/slack/notifiers.js"

puppeteer.use(StealthPlugin())

const EVERY_MINUTE_CRON = "* * * * *";
const EVERY_SIX_HOURS_CRON = "0 */6 * * *";

const browserParams = {
    executablePath: process.env.CHROME_BIN || null,
    args: ['--no-sandbox', '--headless', '--disable-gpu']
}

async function scanAmazon() {
    const browser = await puppeteer.launch(browserParams);
    const page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 1080 });

    const url = "https://www.amazon.com/gp/product/B08FC6MR62";    

    await page.goto(url, { waitUntil: 'load' });
    await page.screenshot({path: 'amazon.png'});

    await page
        .waitForSelector('#availability span', {timeout: 10000})
        .then((element) => amazonFound(element, page, url))
        .catch(() => notifiers.notify("Amazon element not found"))   

    await browser.close();
}

async function scanGameStop() {
    const browser = await puppeteer.launch(browserParams);
    const page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 1080 });
    
    const url = "https://www.gamestop.com/video-games/playstation-5/consoles/products/playstation-5-digital-edition/11108141.html?condition=New";    

    await page.goto(url, { waitUntil: 'load' });
    await page.screenshot({path: 'gamestop.png'});

    await page
        .waitForSelector('button.add-to-cart', {timeout: 10000})
        .then((element) => gameStopFound(element, page, url))
        .catch(() => notifiers.notify("GameStop element not found"))    

    await browser.close();
}

async function amazonFound(element, page, url) {
    // await element.screenshot({path: 'avia.png'});
    const statusText = await page.evaluate(element => element.textContent, element);

    if(statusText.includes("In Stock")) { notifiers.notifyInStock(url); }
    else if(statusText.includes("Currently unavailable")) { /* Do Nothing */ } 
    else { notifiers.notifyDetectError(url); }
}

async function gameStopFound(element, page, url) {
    // await element.screenshot({path: 'avia.png'});
    const statusText = await page.evaluate(element => element.textContent, element);

    if(statusText.includes("Not Available")) { /* Do Nothing */ }
    else { notifiers.notifyInStock(url); }
}

cron.schedule(EVERY_MINUTE_CRON, () => {
    console.log("Scanning for PS5");
    scanAmazon();
    scanGameStop();
});

cron.schedule(EVERY_SIX_HOURS_CRON, () => {
    notifiers.notify("Scanning Amazon");
    scanAmazon();

    notifiers.notify("Scanning GameStop");
    scanGameStop();
});